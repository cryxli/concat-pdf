package li.cryx.tools.concatpdf.io;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * This class is a poor-mans version of the apache commons-io <code>IOUtils</code> only containing the functions needed
 * by this project.
 *
 * @author cryxli
 */
public class IOUtils {

    private IOUtils() {
        // static singleton
    }

    public static void closeQuietly(final Closeable c) {
        if (c != null) {
            try {
                c.close();
            } catch (IOException e) {
                // closeQuietly
            }
        }
    }

    public static void copy(final File in, final OutputStream out) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(in);
            copy(fis, out);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(fis);
        }
    }

    public static void copy(final InputStream in, final File out) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(out);
            copy(in, fos);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(fos);
        }
    }

    public static void copy(final InputStream in, final OutputStream out) {
        try {
            byte[] bytes = new byte[1024];
            int len = in.read(bytes);
            while (len > 0) {
                out.write(bytes, 0, len);
                len = in.read(bytes);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static File createTempFile(final String prefix, final String suffix) {
        try {
            return File.createTempFile(prefix, suffix);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
