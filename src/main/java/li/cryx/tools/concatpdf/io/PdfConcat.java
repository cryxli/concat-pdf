package li.cryx.tools.concatpdf.io;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

/**
 * This class offers a simple way to concatenate PDF documents.
 *
 * @author cryxli
 */
public class PdfConcat implements Closeable {

    /**
     * Create a temporary file and write the merged PDFs to that file.
     * <p>
     * The file can be retrieved using the {@link #getOutFile()} method.
     * </p>
     *
     * @return Instance of {@link PdfConcat} ready to accept PDFs to merge.
     */
    public static PdfConcat toTempFile() {
        final File tmpFile;
        try {
            tmpFile = File.createTempFile("report-", ".pdf");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return new PdfConcat(tmpFile);
    }

    /**
     * Open the output stream to the given file.
     *
     * @param file
     *            File that should contain the final PDF.
     * @return OutputStream pointing to the file.
     */
    private static OutputStream prepareFileForWriting(final File file) {
        try {
            return new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    /** In case of a file output, the location and name of the file. */
    private final File outFile;

    /** In any case the output stream used by iText to write the merged PDF. */
    private final OutputStream outStream;

    /** The iText document used to merge the other PDFs */
    private final Document document;

    /** The iText writer to abstract file access. */
    private final PdfWriter writer;

    /**
     * Create a new instance of the concatenator.
     *
     * @param outFile
     *            Target file that should contain the final merged PDF.
     */
    public PdfConcat(final File outFile) {
        this(outFile, prepareFileForWriting(outFile));
    }

    /**
     * Create a new instance of the concatenator.
     *
     * @param outStream
     *            Output stream containing the final merged PDF.
     */
    public PdfConcat(final OutputStream outStream) {
        this(null, outStream);
    }

    /**
     * Create a new instance of the concatenator.
     *
     * @param outFile
     *            Target file that should contain the final merged PDF. May be <code>null</code>.
     * @param outStream
     *            Output stream containing the final merged PDF. If the <code>outFile</code> is present, this stream is
     *            supposed to write to that file.
     */
    protected PdfConcat(final File outFile, final OutputStream outStream) {
        this.outFile = outFile;
        this.outStream = outStream;

        document = new Document();
        try {
            writer = PdfWriter.getInstance(document, outStream);
        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }
        writer.addViewerPreference(PdfName.PRINTSCALING, PdfName.NONE);
        document.open();
    }

    /**
     * Ensure that the iText document is ready to receive new pages. Throws <code>IllegalStateException</code>,
     * otherwise.
     */
    private void checkDocument() {
        if (!document.isOpen()) {
            throw new IllegalStateException("PdfConcat has already been closed.");
        }
    }

    public void addFile(final File file) {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        IOUtils.copy(file, baos);
        addFile(baos.toByteArray());
    }

    /**
     * Add a PDF to the concatenator.
     *
     * @param pdfByteStream
     *            Byte array containing a PDFs.
     */
    public void addFile(final byte[] pdfByteStream) {
        checkDocument();

        final PdfReader reader;
        try {
            reader = new PdfReader(pdfByteStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        final PdfContentByte cb = writer.getDirectContent();
        for (int index = 1; index <= reader.getNumberOfPages(); index++) {
            document.newPage();
            final PdfImportedPage page = writer.getImportedPage(reader, index);
            cb.addTemplate(page, 0, 0);
        }
        reader.close();
    }

    /**
     * Close the concatenator and finalises the output stream or file. After this method is called no more PDFs can be
     * added and any call to the <code>addFile()</code> method will result in an <code>IllegalStateException</code>.
     * <p>
     * Note that the underlying output stream is not closed, if the instance was created using the stream constructor!
     * </p>
     */
    @Override
    public void close() {
        document.close();
        writer.flush();
        writer.close();
        if (outFile != null) {
            IOUtils.closeQuietly(outStream);
        }
    }

    /**
     * Get the output file, if this instance was created with a file.
     */
    public File getOutFile() {
        return outFile;
    }

    /**
     * Get the output stream, if this instance was created with a stream.
     */
    public OutputStream getOutStream() {
        return outStream;
    }

}
