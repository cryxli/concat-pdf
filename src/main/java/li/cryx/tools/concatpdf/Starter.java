package li.cryx.tools.concatpdf;

import li.cryx.tools.concatpdf.view.MainWindow;

/**
 * Starter class for the "PDF merge" application.
 *
 * @author cryxli
 */
public class Starter {

    public static void main(final String[] args) {
        final MainWindow main = new MainWindow();
        main.show();
    }

}
