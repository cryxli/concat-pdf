package li.cryx.tools.concatpdf.view;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import li.cryx.tools.concatpdf.io.IOUtils;
import li.cryx.tools.concatpdf.io.PdfConcat;
import li.cryx.tools.concatpdf.lang.Translation;
import li.cryx.tools.concatpdf.view.DragAndDropHandler.FileDropTarget;

/**
 * UI main class that also is stuffed with all the application logic.
 *
 * @author cryxli
 */
public class MainWindow implements FileDropTarget {

    private static final GuiLogger LOG = GuiLogger.getInstance();

    private final Translation t = Translation.getInstance();

    private final JFrame frame;

    private final DefaultListModel<String> model = new DefaultListModel<>();

    private PdfConcat concat;

    private final JButton butClose;

    private final JButton butHelp;

    public MainWindow() {
        GuiLogger.registerLoggingTarget(model);

        frame = new JFrame(t.get("app.title"));
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent evt) {
                onQuit();
            }
        });

        final JPanel main = new JPanel(new BorderLayout());
        frame.getContentPane().add(main);
        main.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        main.add(new JScrollPane(new JList<>(model)), BorderLayout.CENTER);

        final JPanel buttons = new JPanel(new BorderLayout());
        main.add(buttons, BorderLayout.SOUTH);
        buttons.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));

        butClose = new JButton(t.get("but.close-pdf"));
        buttons.add(butClose, BorderLayout.EAST);
        butClose.setEnabled(false);
        butClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                onFinalise();
            }
        });

        butHelp = new JButton(t.get("but.help"));
        buttons.add(butHelp, BorderLayout.WEST);
        butHelp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                onHelp();
            }
        });

        frame.setTransferHandler(new DragAndDropHandler(this));
        frame.setSize(400, 300);
    }

    public void onHelp() {
        final Locale locale = Locale.getDefault();

        InputStream stream = getClass()
                .getResourceAsStream("/help/help_" + locale.getLanguage() + "_" + locale.getCountry() + ".htm");
        if (stream == null) {
            stream = getClass().getResourceAsStream("/help/help_" + locale.getLanguage() + ".htm");
        }
        if (stream == null) {
            stream = getClass().getResourceAsStream("/help/help.htm");
        }

        try {
            final File help = IOUtils.createTempFile("help_", ".htm");
            IOUtils.copy(stream, help);
            Desktop.getDesktop().open(help);
        } catch (Exception e) {
            LOG.error(t.get("error.open-help"));
        }
    }

    public void onFinalise() {
        if (concat != null) {
            concat.close();
            try {
                Desktop.getDesktop().open(concat.getOutFile());
                LOG.info(t.get("on.pdf-opened", concat.getOutFile()));
            } catch (IOException e) {
                LOG.error(t.get("error.open-pdf", concat.getOutFile()));
            }
            concat = null;
            butClose.setEnabled(false);
        }
    }

    public void onQuit() {
        LOG.info("Shutting down...");

        onFinalise();

        frame.dispose();
        GuiLogger.registerLoggingTarget(null);
    }

    public void show() {
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        LOG.info(t.get("on.started"));
    }

    @Override
    public void onFileDropped(final File file) {
        model.addElement(t.get("on.file-dropped", file));

        if (!file.getName().toLowerCase().endsWith(".pdf")) {
            LOG.error(t.get("error.not-a-pdf"));
            return;
        }

        boolean isNew = concat == null;
        if (isNew) {
            concat = new PdfConcat(IOUtils.createTempFile("concat_", ".pdf"));
        }

        try {
            concat.addFile(file);
            LOG.msg(t.get("on.appended"));
        } catch (RuntimeException e) {
            LOG.error(t.get("error.cannot-append"));
            e.printStackTrace();
            if (isNew) {
                IOUtils.closeQuietly(concat);
                concat = null;
            }
        }

        butClose.setEnabled(concat != null);
    }

}
