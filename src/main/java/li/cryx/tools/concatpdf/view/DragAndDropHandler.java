package li.cryx.tools.concatpdf.view;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;

/**
 * A transfer handler implementation (Swing's way of handling drag-n-drop) that only accepts files.
 *
 * @author cryxli
 */
public class DragAndDropHandler extends TransferHandler {

    private static final long serialVersionUID = 7210210567316888300L;

    public interface FileDropTarget {

        public abstract void onFileDropped(File file);

    }

    private static final DataFlavor NIX_FILE_LIST_FLAVOR;

    static {
        try {
            NIX_FILE_LIST_FLAVOR = new DataFlavor("text/uri-list;class=java.lang.String");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private final FileDropTarget target;

    public DragAndDropHandler(final FileDropTarget target) {
        this.target = target;
    }

    @Override
    public boolean canImport(final javax.swing.TransferHandler.TransferSupport support) {
        return support.isDataFlavorSupported(java.awt.datatransfer.DataFlavor.javaFileListFlavor)
                || !support.isDataFlavorSupported(NIX_FILE_LIST_FLAVOR);
    }

    @Override
    public boolean importData(final javax.swing.TransferHandler.TransferSupport support) {
        if (!canImport(support)) {
            return false;
        }
        Transferable t = support.getTransferable();
        try {
            try {
                @SuppressWarnings("unchecked")
                final List<Object> files = (List<Object>) t.getTransferData(DataFlavor.javaFileListFlavor);
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        processWindows(files);
                    }
                });
            } catch (java.awt.datatransfer.UnsupportedFlavorException e) {
                final String data = (String) t.getTransferData(NIX_FILE_LIST_FLAVOR);
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        processUnix(data);
                    }
                });
            }
        } catch (java.awt.datatransfer.UnsupportedFlavorException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    private void processUnix(final String data) {
        StringTokenizer st = new StringTokenizer(data, "\r\n");
        do {
            if (!st.hasMoreTokens()) {
                break;
            }
            String token = st.nextToken().trim();
            if (token.startsWith("#") || token.isEmpty()) {
                continue;
            }
            try {
                File file = new File(new URI(token));
                target.onFileDropped(file);
                break;
            } catch (URISyntaxException urisyntaxexception) {
            }
        } while (true);
    }

    private void processWindows(final List<Object> files) {
        Iterator<Object> iterator = files.iterator();
        if (iterator.hasNext()) {
            File file = (File) iterator.next();
            target.onFileDropped(file);
        }
    }

}
