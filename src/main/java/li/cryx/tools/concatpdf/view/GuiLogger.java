package li.cryx.tools.concatpdf.view;

import javax.swing.DefaultListModel;

/**
 * A poor-mans implementation of a logger that sends everything to a <code>JList</code>.
 *
 * @author cryxli
 */
public class GuiLogger {

    public static enum Level {
        DEBUG, INFO, WARN, ERROR,

        /** Special log lever to show messages to users. */
        MSG;
    }

    public DefaultListModel<String> model;

    private Level level = Level.ERROR;

    public static void registerLoggingTarget(final DefaultListModel<String> model) {
        getInstance().model = model;
    }

    public void setLevel(final Level level) {
        if (level != null) {
            this.level = level;
        }
    }

    private static GuiLogger instance = new GuiLogger();

    private GuiLogger() {
    }

    public static GuiLogger getInstance() {
        return instance;
    }

    public void log(final Level level, final String msg) {
        if (level != null && this.level.ordinal() <= level.ordinal()) {
            if (model != null) {
                model.addElement(msg);
            } else {
                System.out.println(msg);
            }
        }
    }

    public void debug(final String msg) {
        log(Level.DEBUG, msg);
    }

    public void info(final String msg) {
        log(Level.INFO, msg);
    }

    public void warn(final String msg) {
        log(Level.WARN, msg);
    }

    public void error(final String msg) {
        log(Level.ERROR, msg);
    }

    public void msg(final String msg) {
        log(Level.MSG, msg);
    }

}
