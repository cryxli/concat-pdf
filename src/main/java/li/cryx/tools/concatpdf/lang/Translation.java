package li.cryx.tools.concatpdf.lang;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Simple static translation factory. It does load the XML properties located in the classpath resource
 * <code>/lang/lang.xml</code> following the same naming conventions as other Java <code>ResourceBundles</code>.
 *
 * <p>
 * The default file <code>/lang/lang.xml</code> is equals to the English variant <code>/lang/lang_en.xml</code>.
 * </p>
 *
 * @author cryxli
 */
public class Translation {

    private static Translation instance = new Translation();

    public static Translation getInstance() {
        return instance;
    }

    private static final String RES_BUNDLE_BASE = "lang/lang";

    private Translation() {
        // static singleton
    }

    public String get(final String key) {
        return get(Locale.getDefault(), key);
    }

    public String get(final String key, final Object... arguments) {
        return MessageFormat.format(get(key), arguments);
    }

    public String get(final Locale locale, final String key) {
        try {
            return ResourceBundle
                    .getBundle(RES_BUNDLE_BASE, locale, getClass().getClassLoader(), new XMLResourceBundleControl())
                    .getString(key);
        } catch (MissingResourceException e) {
            return "XXX " + key + " XXX";
        }
    }

}
